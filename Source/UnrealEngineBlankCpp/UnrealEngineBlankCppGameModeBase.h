// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "UnrealEngineBlankCppGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class UNREALENGINEBLANKCPP_API AUnrealEngineBlankCppGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};

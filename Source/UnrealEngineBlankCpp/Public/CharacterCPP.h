// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"

#include "GameFramework/Character.h"
#include "CharacterCPP.generated.h"

UCLASS()
class UNREALENGINEBLANKCPP_API ACharacterCPP : public ACharacter {
  GENERATED_BODY()
  // Spring arm component which will act as a
  // placeholder for
  // the player camera. This component is recommended to
  // be used as it automatically controls how the
  // camera handles situations
  // where it becomes obstructed by geometry inside the
  // level, etc
  UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = MyTPS_Cam,
            meta = (AllowPrivateAccess = "true"))
  class USpringArmComponent *CameraBoom;
  // Follow camera
  UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = MyTPS_Cam,
            meta = (AllowPrivateAccess = "true"))
  class UCameraComponent *FollowCamera;

public:
  // Sets default values for this character's properties
  ACharacterCPP();

protected:
  // Called when the game starts or when spawned
  virtual void BeginPlay() override;

public:
  // Called every frame
  virtual void Tick(float DeltaTime) override;

  // Called to bind functionality to input
  virtual void SetupPlayerInputComponent(
      class UInputComponent *PlayerInputComponent) override;
};

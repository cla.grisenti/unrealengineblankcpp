// Copyright Epic Games, Inc. All Rights Reserved.

#include "UnrealEngineBlankCpp.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, UnrealEngineBlankCpp, "UnrealEngineBlankCpp" );
